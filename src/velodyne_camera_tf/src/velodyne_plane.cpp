#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_broadcaster.h>
#include <pcl_msgs/ModelCoefficients.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

ros::Publisher pub_cloud, pub_coeff;


void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  static tf::TransformBroadcaster plane_to_velo_tf;
  // Create a container for the data.
  sensor_msgs::PointCloud2 output;

  // Tf not required here right now - moved it to launch file
  // tf::TransformBroadcaster velo_tf;
  
  // Container for original & filtered data

  pcl::PCLPointCloud2::Ptr cloud_blob(new pcl::PCLPointCloud2), cloud_plane(new pcl::PCLPointCloud2);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_b (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>);


  // Convert to PCL data type
  pcl_conversions::toPCL(*input, *cloud_blob);
  // Convert to the templated PointCloud
  pcl::fromPCLPointCloud2 (*cloud_blob, *cloud_b);


  // Perform distance filtering - this part is icky. 
  pcl::PassThrough<pcl::PointXYZ> pass;
  pass.setInputCloud (cloud_b);
  pass.setFilterFieldName ("x");
  pass.setFilterLimits (0.0, 2.8);
  pass.filter (*cloud_f);
  cloud_b.swap(cloud_f);
  pass.setInputCloud (cloud_b);
  pass.setFilterFieldName ("y");
  pass.setFilterLimits (-1.5, 1.5);
  pass.filter (*cloud_f);
  cloud_b.swap(cloud_f);
  pass.setInputCloud (cloud_b);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (-2.5, 2.5);
  pass.filter (*cloud_f);
  cloud_b.swap(cloud_f);

  // std::vector<int> indices;
  // pcl::removeNaNFromPointCloud(*cloud_b,*cloud_b, indices);
  // ROS_INFO("Number of NANs = %lu", indices.size());

  // Smooth out the point cloud noise
  // Create a KD-Tree
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

  // Init object (second point type is for the normals, even if unused)
  pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;

  // Set parameters
  mls.setInputCloud (cloud_b);
  mls.setPolynomialFit (true);
  mls.setSearchMethod (tree);
  mls.setSearchRadius (0.02);

  // Reconstruct
  mls.process (*cloud_f);

  if(cloud_f->points.size() < 20){
    ROS_INFO("To few points after smoothing");
  }
  else{
    // Perform the plane filtering
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold (0.0001);
    seg.setMaxIterations(1000);

    seg.setInputCloud (cloud_f);
    seg.segment (*inliers, *coefficients);

    if (inliers->indices.size() == 0){
      ROS_INFO("No plane found");
    }
    else{

    // Publish the model coefficients
      pcl_msgs::ModelCoefficients ros_coefficients;
      pcl_conversions::fromPCL(*coefficients, ros_coefficients);
      pub_coeff.publish (ros_coefficients);

    // Extract the plane
      pcl::ExtractIndices<pcl::PointXYZ> extract;
      extract.setInputCloud(cloud_f);
      extract.setIndices(inliers);
      extract.setNegative(true);
      extract.filter(*cloud_p);
    // Convert to ROS data type
      pcl::toPCLPointCloud2 (*cloud_p, *cloud_plane);
      pcl_conversions::fromPCL(*cloud_plane, output);

    // Publish the data
      pub_cloud.publish (output);

    // Find out the normal and display it
      tf::Transform plane_to_velo;
      plane_to_velo.setOrigin(tf::Vector3(cloud_p->points[0].x, cloud_p->points[0].y, cloud_p->points[0].z));
      tf::Vector3 normal_vector(-1.0*coefficients->values[2], -1.0*coefficients->values[1], coefficients->values[0]);
      tf::Vector3 up_vector(0.0, 0.0, 1.0);
      tf::Vector3 right_vector = normal_vector.cross(up_vector);
      right_vector.normalized();
      tf::Quaternion q(right_vector, -1.0*acos(normal_vector.dot(up_vector)));
      q.normalize();
      plane_to_velo.setRotation(q);
      plane_to_velo_tf.sendTransform(tf::StampedTransform(plane_to_velo, ros::Time::now(), "/velodyne", "/plane"));

    }
  }
  
}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "velodyne_to_plane");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/sensors/velodyne_points", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  pub_cloud = nh.advertise<sensor_msgs::PointCloud2> ("velodyne_plane_points", 1);
  pub_coeff = nh.advertise<pcl_msgs::ModelCoefficients> ("velodyne_plane_coefficients", 1);

  // Spin
  ros::spin ();
}