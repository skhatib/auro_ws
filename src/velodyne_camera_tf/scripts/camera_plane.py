#!/usr/bin/env python
import sys
import rospy
import cv2
import tf
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
from image_geometry import PinholeCameraModel
from message_filters import ApproximateTimeSynchronizer, Subscriber

class image_converter:

	def __init__(self):
		self.image_pub = rospy.Publisher("/sensors/camera/image_plane_tf",Image, queue_size=5)
		self.bridge = CvBridge()
		self.cam_model = PinholeCameraModel()
		self.tss = ApproximateTimeSynchronizer([Subscriber("/sensors/camera/image_rect_color", Image), 
				       				Subscriber("sensors/camera/camera_info", CameraInfo)], 10, 1)
		self.tss.registerCallback(self.im_callback)
		# self.im_sub = rospy.Subscriber("sensors/camera/image_rect_color", Image, im_callback)
		# self.cam_info_sub = rospy.Subscriber("camera_info2", CameraInfo, info_callback)
		self.checkerboard = (7,5)
		self.size = 0.05
		self.listener = tf.TransformListener()
		# self.cam_received = False

	def im_callback(self, image, cam_info):
		try:
			cv_image = self.bridge.imgmsg_to_cv2(image, "bgr8")
			self.cam_model.fromCameraInfo(cam_info)
		except CvBridgeError as e:
			print e

		# Since image is already calibrated, do need distortion here - make sure correct topic
		# cv2.imshow("Image Window", cv_image)
		# cv2.waitKey(1)
		cv_image_gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
		# cam_matrix = self.cam_model.intrinsicMatrix()
		# dist = self.cam_model.distortionCoeffs()
		cam_matrix = np.array([[482.597531, 0.000000, 455.813184],
							   [0.000000, 482.759745, 364.540275],
							   [0.000000, 0.000000, 1.000000]])
		criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.01)
		objp = np.zeros((self.checkerboard[0]*self.checkerboard[1],3), np.float32)
		objp[:,:2] = np.mgrid[0:self.checkerboard[0],0:self.checkerboard[1]].T.reshape(-1,2)
		axis = np.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)


		ret, corners = cv2.findChessboardCorners(cv_image_gray, self.checkerboard, None )
		if ret == True:
			# corners2 = cv2.cornerSubPix(cv_image_gray, corners,(5,5),(-1,-1),criteria)
			# Find the rotation and translation vectors.
			rvecs, tvecs, inliers = cv2.solvePnPRansac(objp, corners, cam_matrix, None)

			# print "Before Left to right hand correction"
			# print rvecs
			# Left hand to right hand rotation
			LtoR_mat = np.zeros((3,3), np.float32)
			LtoR_mat[0,0] = 1
			LtoR_mat[1,1] = 1
			LtoR_mat[2,2] = -1

			# Plane at origin
			rvecs_mat, _ = cv2.Rodrigues(rvecs)
			tvecs_scale = self.size*tvecs 

			# inverse transformations - Camera at origin
			# rvecs_mat_T = rvecs_mat.transpose()
			# tvecs_T = -1.0 * rvecs_mat_T.dot(tvecs)
			# Need another 90 degree rotation around z-axis clock wise to stick to convention
			rot_mat = np.zeros((3,3), np.float32)
			rot_mat[0,1] = -1
			rot_mat[1,0] = 1
			rot_mat[2,2] = 1

			# print tvecs
			rvecs_mat = rvecs_mat.dot(rot_mat)
			rvecs_mat = rvecs_mat.dot(LtoR_mat)

			euler = tf.transformations.euler_from_matrix(rvecs_mat, 'rxyz')

			br = tf.TransformBroadcaster()
			br.sendTransform((tvecs_scale[0], tvecs_scale[1], tvecs_scale[2]),
			                 tf.transformations.quaternion_from_euler(euler[0], euler[1], euler[2], 'rxyz'),
			                 rospy.Time.now(),
			                 "/camera_optical",
			                 "/plane")

			# rvecs_mat, _ = cv2.Rodrigues(rvecs)
			# # print "Before Left to right hand correction"
			# # print rvecs_mat

			# rvecs_R_mat = rvecs_mat * LtoR_mat

			# rvecs_R, _ = cv2.Rodrigues(rvecs_R_mat)

			# print "After right hand correction"
			# print rvecs_R

			# project 3D points to image plane
			imgpts, jac = cv2.projectPoints(axis, rvecs, tvecs, cam_matrix, None)
			# print imgpts
			# print corners
			# cv2.drawChessboardCorners(cv_image, self.checkerboard, corners2, ret)
			cv_image = self.draw(cv_image, corners, imgpts)
			# try:
			# 	cv2.imshow('Image Window', cv_image)
			# 	cv2.waitKey(1)
			# except:
			# 	rospy.loginfo("Image error")

		# (rows,cols,channels) = cv_image.shape
		# if cols > 60 and rows > 60 :
			# cv2.circle(cv_image, (50,50), 10, 255)

	    	# cv2.imshow("Image window", cv_image)
	    	# cv2.waitKey(3)

		try:
			self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
		except CvBridgeError as e:
			print e
		
		try:
			(trans,rot) = self.listener.lookupTransform('/velodyne', '/camera_optical', rospy.Time(0))
		except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
  			# rospy.loginfo("Transfrom from velodyne to camera not yet available")
  			pass

	def draw(self, img, corners, imgpts):
		corner = tuple(corners[0].ravel())
		cv2.line(img, corner, tuple(imgpts[0].ravel()), (0,0,255), 3)
		cv2.line(img, corner, tuple(imgpts[1].ravel()), (0,255,0), 3)
		cv2.line(img, corner, tuple(imgpts[2].ravel()), (255,0,0), 3)
		return img

def main(args):
	rospy.init_node('image_converter', anonymous=True)
	pose_tf = tf.TransformListener()
	try:
		pose_tf.waitForTransform("/velodyne", "/plane", rospy.Time(), rospy.Duration(4.0))
	except:
		rospy.loginfo("TF from /velodyne to /plane failed")
	rospy.loginfo("/plane to has been established")
	ic = image_converter()
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
		cv2.destroyAllWindows()

if __name__ == '__main__':
	main(sys.argv)
