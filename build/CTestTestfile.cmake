# CMake generated Testfile for 
# Source directory: /home/shehzi/indigo/auro_ws/src
# Build directory: /home/shehzi/indigo/auro_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(camera_calib_rect)
SUBDIRS(color_velodyne)
SUBDIRS(velodyne_camera_tf)
