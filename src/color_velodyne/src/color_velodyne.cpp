#include <opencv/cv.h>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_geometry/pinhole_camera_model.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <pcl_msgs/ModelCoefficients.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>

void project_cloud(cv::Rect, pcl::PointCloud<pcl::PointXYZ>::Ptr, pcl::PointCloud<pcl::PointXYZRGB>::Ptr);


ros::Publisher pub;
image_geometry::PinholeCameraModel cam_model;

cv::Mat frame_rgb;

void cam_info_cb(const sensor_msgs::CameraInfoConstPtr& msg)
{ 
  cam_model.fromCameraInfo(msg);
}

void image_cb(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  frame_rgb = cv_ptr->image;
}

void pointCloud_cb(const sensor_msgs::PointCloud2ConstPtr& msg)
{

  // if no rgb frame for coloring:
  if (frame_rgb.data == NULL)
  {
    return;
  }

  pcl::PCLPointCloud2::Ptr cloud_blob(new pcl::PCLPointCloud2);
  pcl::PCLPointCloud2::Ptr color_cloud_blob(new pcl::PCLPointCloud2);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_b (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_b_t (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr visible_points (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr visible_points_t (new pcl::PointCloud<pcl::PointXYZRGB>);
  sensor_msgs::PointCloud2 color_cloud;
  // Convert to PCL data type
  pcl_conversions::toPCL(*msg, *cloud_blob);
  // Convert to the templated PointCloud
  pcl::fromPCLPointCloud2 (*cloud_blob, *cloud_b);

  // x := x, y := -z, z := y,
  // Eigen::Affine3f transf = pcl::getTransformation(0, 0, 0, M_PI / 2, 0, 0);
  Eigen::Affine3f transf;
  Eigen::Affine3d transd;
  // Define a translation of 2.5 meters on the x axis.
  // transf.translation() << 2.5, 0.0, 0.0;
  // pcl::transformPointCloud(*cloud_b, *cloud_b_t, transf);
  // cloud_b.swap(cloud_b_t);
  tf::Transform cam_velo_tf;
  // Figure out a better way to get this transform!
  cam_velo_tf.setOrigin(tf::Vector3(0.255, -0.340, -0.591));
  cam_velo_tf.setRotation(tf::Quaternion(0.569, -0.536, 0.424, 0.458));
  // cam_velo_tf.setOrigin(tf::Vector3(0.001, -1.239, -1.077));
  // cam_velo_tf.setRotation(tf::Quaternion(0.560, -0.486, 0.339, 0.578));
  tf::transformTFToEigen(cam_velo_tf, transd);
  transf = transd.cast<float>();
  pcl::transformPointCloud(*cloud_b, *cloud_b_t, transf);
  cloud_b.swap(cloud_b_t);
  transf = pcl::getTransformation(0, 0, 0, 0 , -M_PI , 0);
  pcl::transformPointCloud(*cloud_b, *cloud_b_t, transf);
  project_cloud(cv::Rect(0, 0, frame_rgb.cols, frame_rgb.rows), cloud_b_t, visible_points);

  // reverse axix switching:
  // transf = pcl::getTransformation(0, 0, 0, - M_PI / 2, 0, M_PI/2);
  transf = pcl::getTransformation(0, 0, 0, 0, -M_PI, 0);
  pcl::transformPointCloud(*visible_points, *visible_points_t, transf);
  visible_points.swap(visible_points_t);
  tf::transformTFToEigen(cam_velo_tf.inverse(),transd);
  transf = transd.cast<float>();
  pcl::transformPointCloud(*visible_points, *visible_points_t, transf);
  // Convert to PCL data type
  pcl::toPCLPointCloud2 (*visible_points_t, *color_cloud_blob);
  // Convert to the templated PointCloud
  pcl_conversions::fromPCL(*color_cloud_blob, color_cloud);

  color_cloud.header = msg->header;
  pub.publish(color_cloud);
}

void project_cloud(cv::Rect frame, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZRGB>::Ptr visible_points){
  pcl::PointXYZRGB temp;
  for (size_t i = 0; i < cloud->points.size(); ++i)
  {

    // behind the camera or NAN points
    if (cloud->points[i].z > 0 || isnan (cloud->points[i].x) || isnan (cloud->points[i].y) || isnan (cloud->points[i].z))
    {
      continue;
    }

    cv::Point3d pt_cv(cloud->points[i].x, cloud->points[i].y, cloud->points[i].z);
    cv::Point xy = cam_model.project3dToPixel(pt_cv);
    if (xy.inside(frame))
    {
      if (visible_points != NULL)
      { 
        temp.x = cloud->points[i].x;
        temp.y = cloud->points[i].y;
        temp.z = cloud->points[i].z;
        cv::Vec3b bgr = frame_rgb.at<cv::Vec3b>(xy);
        temp.r = bgr.val[2];
        temp.g = bgr.val[1];
        temp.b = bgr.val[0];
        visible_points->points.push_back(temp);
      }
    }
  }
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "coloring_node");

  ros::NodeHandle n;
  // n.getParam("/but_calibration_camera_velodyne/camera_frame_topic", CAMERA_FRAME_TOPIC);
  // n.getParam("/but_calibration_camera_velodyne/camera_info_topic", CAMERA_INFO_TOPIC);
  // n.getParam("/but_calibration_camera_velodyne/velodyne_topic", VELODYNE_TOPIC);
  // n.getParam("/but_calibration_camera_velodyne/velodyne_color_topic", VELODYNE_COLOR_TOPIC);
  // n.getParam("/but_calibration_camera_velodyne/6DoF", DoF);

  pub = n.advertise<sensor_msgs::PointCloud2>("color_velodyne", 1);

  // Subscribe input camera image
  ros::Subscriber cam_sub = n.subscribe<sensor_msgs::Image>("sensors/camera/image_rect_color", 1, image_cb);
  ros::Subscriber cam_info_cub = n.subscribe<sensor_msgs::CameraInfo>("camera_info2", 1, cam_info_cb);

  ros::Subscriber pc_sub = n.subscribe<sensor_msgs::PointCloud2>("sensors/velodyne_points", 1, pointCloud_cb);

  ros::spin();

  return EXIT_SUCCESS;
}
